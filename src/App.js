import React from 'react';
import logo from './logo.svg';
import './App.css';
import './assets/css/style.css';
import { BrowserRouter as Router } from "react-router-dom";
import Routes from './Routes';
import Nav from './components/Nav';

function App() {
  return (
    <div className="App">
      {/* <header> */}
      <img id="logo" src={process.env.PUBLIC_URL + '/img/logo.png'} width="200px" />
      <Router>
    <Nav></Nav>
        <Routes />
      </Router> 
    {/* </header> */}
    
    <footer>
      <h5>copyright &copy; 2020 by Sanbercode</h5>
    </footer>
    </div>
  );
}

export default App;
