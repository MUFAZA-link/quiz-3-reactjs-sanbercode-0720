import React from "react";
import About from "./components/About";
import Home from "./components/Home/Home";
import { Switch, Route } from "react-router";
import Nav from "./components/Nav";

export default function App() {

  return (
    <>
    <Switch>
      <Route exact path="/">
        <Home />
      </Route>
      <Route path="/about">
        <About />
      </Route>
      <Route exact path="/movies">
        {/* <Dashboard /> */}
      </Route>
      <Route exact path="/login">
        {/* <Dashboard /> */}
      </Route>
    </Switch>
    </>
  );
};
